class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :title
      t.text :description
      t.string :isbn
      t.date :published_at
      t.integer :pages
      t.decimal :price
      t.integer :quantity, default: 0

      t.timestamps
    end
  end
end
