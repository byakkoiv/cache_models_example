class BooksController < ApplicationController
  include BookHelper
  before_action :set_book, except: [:index, :create, :new]
  def index
    @books = fetch_books
  end

  def show
  end

  def new
    @book = Book.new
  end

  def create
    @book = Book.new(book_params)
    if @book.save
      flash[:notice] = "Book has been created"
      redirect_to @book
    else
      flash[:error] = "Book has not been created"
      render :new
    end
  end

  def edit
  end

  def update
    if @book.update(book_params)
      flash[:notice] = "Book has been updated"
      redirect_to @book
    else
      flash[:error] = "Book has not been updated"
      render :new
    end
  end

  def destroy
    @book.destroy
    flash[:notice] = "Book has been deleted"
    redirect_to books_path
  end

  private
  def set_book
    @book = Book.find(params[:id])
  end

  def book_params
    params.require(:book).permit(:title, :description, :isbn, :publisshed_at, :pages, :price, :quantity)
  end
end
