class CategoriesController < ApplicationController
  before_action :set_category, except: [:index, :create, :new]
  def index
    @categories = Category.all
  end

  def show
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:notice] = "Category has been created"
      redirect_to @category
    else
      flash[:error] = "Category has not been created"
      render :new
    end
  end

  def edit
  end

  def update
    if @category.update(category_params)
      flash[:notice] = "Category has been updated"
      redirect_to @category
    else
      flash[:error] = "Category has not been updated"
      render :new
    end
  end

  def destroy
    @category.destroy
    flash[:notice] = "Category has been deleted"
    redirect_to categories_path
  end

  private
  def set_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:name, :description)
  end
end
