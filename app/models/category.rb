class Category < ApplicationRecord
  validates :name, presence: true, length: { minimum: 4, maximum: 50 }
  validates :description, presence: true, length: { minimum: 15, maximum: 400 }
end
