class Book < ApplicationRecord
  after_save :clear_cache
  validates :title, :description, :isbn, :price, presence: true
  validates :pages, numericality: { only_integer: true }
  validates :price, :quantity, numericality: { greater_than_or_equal_to: 0 }


  def clear_cache
    $redis.del 'books'
  end
end
