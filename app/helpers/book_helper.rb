module BookHelper
  def fetch_books
    books = $redis.get("books")
    if books.nil?
      books = Book.all.to_json
      $redis.set("books", books)
      $redis.expire("books", 60.minutes.to_i)
    end
    @books = JSON.load books
  end
end
