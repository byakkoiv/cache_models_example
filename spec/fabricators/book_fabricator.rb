Fabricator(:book) do
  title { Faker::Book.title }
  description { Faker::Lorem.paragraph }
  isbn  '11111111111'
  published_at { Date.new }
  pages 100
  price { Faker::Commerce.price }
  quantity 1
end
