Fabricator(:category) do
  name  { Faker::Commerce.department(2, true) }
  description { Faker::Lorem.paragraph(2, true) }
end
