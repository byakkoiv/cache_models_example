require 'rails_helper'

RSpec.feature "ListingCategories", type: :feature do
  let!(:category1) { Fabricate(:category, name: "Child tales") }
  let!(:category2) { Fabricate(:category, name: "Terror") }

  scenario "listing category in database" do
    visit root_path

    click_link "Categories"
    within('#categories-list') do
      expect(page).to have_content('Child tales')
      expect(page).to have_content('Terror')
    end
  end

  scenario "with none category created" do
    Category.delete_all

    visit root_path

    click_link "Categories"
    expect(Category.count).to eq(0)
  end
end
