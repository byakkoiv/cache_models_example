require 'rails_helper'

RSpec.feature "ListingBooks", type: :feature do
  let!(:book1) { Fabricate(:book, title: "Game of thrones") }
  let!(:book2) { Fabricate(:book, title: "Lord of the rings") }

  scenario "listing book in database" do
    visit root_path

    click_link "Books"
    within('#books-list') do
      expect(page).to have_content('Game of thrones')
      expect(page).to have_content('Lord of the rings')
    end
  end

  scenario "with none book created" do
    Book.delete_all

    visit root_path

    click_link "Books"
    expect(Book.count).to eq(0)
  end
end
