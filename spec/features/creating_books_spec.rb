require 'rails_helper'

RSpec.feature "CreatingBooks", type: :feature do
  scenario "Creating a new book" do
    visit root_path
    click_link "Books"
    click_link "New book"

    fill_in "Title", with: "The end of the work"
    fill_in "Description", with: "Ipsum modi expedita fugiat nostrum accusamus eum culpa praesentium sed Deleniti accusantium modi eveniet"
    fill_in "Isbn", with: "1234567890"
    fill_in "Published at", with: 1.year.ago
    fill_in "Pages", with: 900
    fill_in "Price", with: 500
    fill_in "Quantity", with: 3

    click_button "Save"
    expect(page).to have_content("Book has been created")
  end
  scenario "Not create a category with invalid inputs" do
    visit root_path
    click_link "Books"
    click_link "New book"

    fill_in "Title", with: nil
    fill_in "Description", with: "Ipsum modi expedita fugiat nostrum accusamus eum culpa praesentium sed Deleniti accusantium modi eveniet"
    fill_in "Isbn", with: "1234567890"
    fill_in "Published at", with: 1.year.ago
    fill_in "Pages", with: 900
    fill_in "Price", with: 500
    fill_in "Quantity", with: 3

    click_button "Save"
    expect(page).to have_content("Book has not been created")
  end
end
