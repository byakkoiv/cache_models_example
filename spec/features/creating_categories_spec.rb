require 'rails_helper'

RSpec.feature "CreatingCategories", type: :feature do
  scenario "Creating a new category" do
    visit root_path
    click_link "Categories"
    click_link "New Category"

    fill_in "Name", with: "My category"
    fill_in "Description", with: "Ipsddum modi expedita fugiat nostrum accusamus eum culpa praesentium sed Deleniti accusantium modi eveniet"

    click_button "Save"
    expect(page).to have_content("Category has been created")
  end
  scenario "Not create a category with invalid inputs" do
    visit root_path
    click_link "Categories"
    click_link "New Category"

    fill_in "Name", with: nil
    fill_in "Description", with: nil

    click_button "Save"
    expect(page).to have_content("Category has not been created")
  end
end
