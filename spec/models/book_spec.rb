require 'rails_helper'

RSpec.describe Book, type: :model do
  it { should validate_presence_of :title }
  it { should validate_presence_of :description }
  it { should validate_presence_of :isbn }
  it { should validate_presence_of :price }
  it { should validate_numericality_of(:pages).only_integer }
  it { should validate_numericality_of(:price).is_greater_than_or_equal_to(0) }
  it { should validate_numericality_of(:quantity).is_greater_than_or_equal_to(0) }
end
