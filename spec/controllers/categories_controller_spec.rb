require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
  describe "GET #index" do
    it "returns a successful http request status code" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    let(:category) { Fabricate(:category) }
    it "returns a successful http reques status code" do
      get :show, params: { id: category.id }
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #new" do
    it "returns a successful http request status code" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST #create" do
    context "a successful create" do
      it "saves the new category object with valid inputs" do
        post :create, params: { category: Fabricate.attributes_for(:category) }
        expect(Category.count).to eq(1)
      end

      it "set success flash massage" do
        post :create, params: { category: Fabricate.attributes_for(:category) }
        expect(flash[:notice]).to eq("Category has been created")
      end
      
      it "redirects to the category show action" do
        post :create, params: { category: Fabricate.attributes_for(:category) }
        expect(response).to redirect_to category_path(Category.last)
      end
    end
    
    context "an unsuccessful create" do
      it "does not save the new category object with invalid inputs" do
        post :create, params: { category: Fabricate.attributes_for(:category, name: nil) }
        expect(Category.count).to eq(0)
      end

      it "set failure flash massage" do
        post :create, params: { category: Fabricate.attributes_for(:category, name: nil) }
        expect(flash[:error]).to eq("Category has not been created")
      end
    end
  end

  describe "GET #edit" do
    let(:category) { Fabricate(:category) }

    it "returns a successful http reques status code" do
      get :edit, params: { id: category.id }
      expect(response).to have_http_status(:success)
    end
  end

  describe "PUT #update" do
    context "successful update" do
      let(:category) { Fabricate(:category, name: "Action") }
      it "updates modified category object" do
        put :update, params: { category: Fabricate.attributes_for(:category, name: "Terror"), id: category.id }
        expect(Category.last.name).to eq("Terror")
        expect(Category.last.name).not_to eq("Action")
      end
      
      it "sets the sucessful flash message" do
        put :update, params: { category: Fabricate.attributes_for(:category, name: "Terror"), id: category.id }
        expect(flash[:notice]).to eq("Category has been updated")
      end

      it "redirects to the show action" do
        put :update, params: { category: Fabricate.attributes_for(:category, name: "Terror"), id: category.id }
        expect(response).to redirect_to category_path(Category.last)
      end
    end

    context "unsuccessful update" do
      let(:category) { Fabricate(:category, name: "Action") }
      it "does not updates modified category object with invalid inputs" do
        put :update, params: { category: Fabricate.attributes_for(:category, name: nil), id: category.id }
        expect(Category.last.name).to eq("Action")
      end
      
      it "sets the sucessful flash message" do
        put :update, params: { category: Fabricate.attributes_for(:category, name: nil), id: category.id }
        expect(flash[:error]).to eq("Category has not been updated")
      end
    end
  end

  describe "DELETE #destroy" do
    let(:category) { Fabricate(:category) }

    it "deletes the category with the given id" do
      delete :destroy, params: { id: category.id }
      expect(Category.count).to eq(0)
    end

    it "sets the flash message" do
      delete :destroy, params: { id: category.id }
      expect(flash[:notice]).to eq("Category has been deleted")
    end

    it "redirects to the index action" do
      delete :destroy, params: { id: category.id }
      expect(response).to redirect_to categories_path
    end
  end
end
