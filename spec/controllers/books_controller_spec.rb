require 'rails_helper'

RSpec.describe BooksController, type: :controller do
  describe "GET #index" do
    it "returns a successful http request status code" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    let(:book) { Fabricate(:book) }
    it "returns a successful http reques status code" do
      get :show, params: { id: book.id }
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #new" do
    it "returns a successful http request status code" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST #create" do
    context "a successful create" do
      it "saves the new book object with valid inputs" do
        post :create, params: { book: Fabricate.attributes_for(:book) }
        expect(Book.count).to eq(1)
      end

      it "set success flash massage" do
        post :create, params: { book: Fabricate.attributes_for(:book) }
        expect(flash[:notice]).to eq("Book has been created")
      end
      
      it "redirects to the book show action" do
        post :create, params: { book: Fabricate.attributes_for(:book) }
        expect(response).to redirect_to book_path(Book.last)
      end
    end
    
    context "an unsuccessful create" do
      it "does not save the new book object with invalid inputs" do
        post :create, params: { book: Fabricate.attributes_for(:book, title: nil) }
        expect(Book.count).to eq(0)
      end

      it "set failure flash massage" do
        post :create, params: { book: Fabricate.attributes_for(:book, title: nil) }
        expect(flash[:error]).to eq("Book has not been created")
      end
    end
  end

  describe "GET #edit" do
    let(:book) { Fabricate(:book) }

    it "returns a successful http reques status code" do
      get :edit, params: { id: book.id }
      expect(response).to have_http_status(:success)
    end
  end

  describe "PUT #update" do
    context "successful update" do
      let(:book) { Fabricate(:book, title: "Action") }
      it "updates modified book object" do
        put :update, params: { book: Fabricate.attributes_for(:book, title: "Terror"), id: book.id }
        expect(Book.last.title).to eq("Terror")
        expect(Book.last.title).not_to eq("Action")
      end
      
      it "sets the sucessful flash message" do
        put :update, params: { book: Fabricate.attributes_for(:book, title: "Terror"), id: book.id }
        expect(flash[:notice]).to eq("Book has been updated")
      end

      it "redirects to the show action" do
        put :update, params: { book: Fabricate.attributes_for(:book, title: "Terror"), id: book.id }
        expect(response).to redirect_to book_path(Book.last)
      end
    end

    context "unsuccessful update" do
      let(:book) { Fabricate(:book, title: "Action") }
      it "does not updates modified book object with invalid inputs" do
        put :update, params: { book: Fabricate.attributes_for(:book, title: nil), id: book.id }
        expect(Book.last.title).to eq("Action")
      end
      
      it "sets the sucessful flash message" do
        put :update, params: { book: Fabricate.attributes_for(:book, title: nil), id: book.id }
        expect(flash[:error]).to eq("Book has not been updated")
      end
    end
  end

  describe "DELETE #destroy" do
    let(:book) { Fabricate(:book) }

    it "deletes the book with the given id" do
      delete :destroy, params: { id: book.id }
      expect(Book.count).to eq(0)
    end

    it "sets the flash message" do
      delete :destroy, params: { id: book.id }
      expect(flash[:notice]).to eq("Book has been deleted")
    end

    it "redirects to the index action" do
      delete :destroy, params: { id: book.id }
      expect(response).to redirect_to books_path
    end
  end
end
